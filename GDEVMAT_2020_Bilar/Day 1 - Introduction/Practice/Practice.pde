//Declare Variables
//int x;
//int y;


//Gets called when the program runs
void setup()
{
  //For the Mac
  size(1920, 1080, P3D);
  
  //Initialize Variable
  //x = width / 2;
  //y = height / 2;
  
  //To remove the need to divide width and height
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  
  //For the Laptop
  //size(1080, 720, P3D);
}


//Gets called every frame
void draw()
{
  background(180);
  drawCircle();
  drawCartesianPlane();
  drawLinearFunction();
  drawQuadraticFunction();
  drawSineWave();
}

float radius = 50;

void drawCircle()
{
   for(int x = 0; x < 360; x++)
   {
      circle((float)Math.cos(x) * radius, (float)Math.sin(x) * radius, 1);
   }
}

void drawCartesianPlane()
{
  //Vertical Line
  line(300, 0, -300, 0);
  
  //Horizontal Line
  line(0, 300, 0, -300);
  
  //Draw the lines in the Cartesian Plane
  for (int i = -300; i <= 300; i += 10)
  {
     line(i, -5, i, 5);
     line(-5, i, 5, i);
  } 
}

void drawLinearFunction()
{
   /* 
     f(x) = x + 2
     Let x be 4, then y = 6 (4, 6)
     Let x be -5, then y = -3 (-5, -3)
   */
   
   for(int x = -200; x <= 200; x++)
   {
      circle(x, x + 2, 1); 
   }
}

void drawQuadraticFunction()
{
  /* 
     f(x) = x ^ 2 + 2x - 5
     Let x be 2, then y = 3
     Let x be -2, then y = 3
     Let x be -1, then y = -6
   */
   
    for(float x = -300; x <= 300; x += 0.1)
   {
      circle(x * 10, (x * x) + ( 2 * x) - 5, 1);
   }
}




void drawSineWave()
{
   circle(0, 0, 50);
}
