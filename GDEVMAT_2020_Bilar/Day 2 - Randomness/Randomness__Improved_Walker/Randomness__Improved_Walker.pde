Walker walker = new Walker();

void setup()
{
  //For the Mac
  size(1920, 1080, P3D);
  
  //To remove the need to divide width and height
  camera(0, 0, -(height / 2) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  
  background(130);
}


//Gets called every frame
void draw()
{
  walker.render();
  walker.walk();
}
