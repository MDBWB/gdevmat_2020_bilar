class Walker
{
  float xPosition;
  float yPosition;
  float speed = 5;
  
  //default constructor
  Walker()
  {
    xPosition = 0;
    yPosition = 0;
  }
  
  Walker(float x, float y)
  {
    xPosition = x;
    yPosition = y;
  }
  
  //Draws the Walker
  void render()
  {
    circle(xPosition, yPosition, 30);
    fill(random(0, 255), random(0, 255), random(0, 255));
    stroke(0);
  }
  
  void walk()
  {
    int direction = floor(random(8));
    
    //Up - Down - Right - Left Movement
    if (direction == 0)
    {
       xPosition += speed;
    }
    else if (direction == 1)
    {
       xPosition -= speed;
    }
    else if (direction == 2)
    {
       yPosition += speed;
    }
    else if (direction == 3)
    {
       yPosition -= speed;
    }
    //Diagonal Movement
    else if (direction == 4)
    {
       xPosition += speed;
       yPosition += speed;
    }
    else if (direction == 5)
    {
      xPosition += speed;
      yPosition -= speed;
    }
    else if (direction == 6)
    {
      xPosition -= speed;
      yPosition -= speed;
    }
    else if (direction == 7)
    {
       xPosition -= speed;
       yPosition += speed;
    }
  }
}
