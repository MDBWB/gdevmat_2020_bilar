float Xpos;
float Ypos;
float Csize;

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, -(height  / 2) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
  background(130);
}

void draw()
{
  drawGaussian();
  SketchReset();
  println(frameCount);
}

//Draws the Circle [ Paint Splatter ] w / random pos, size, and color
void drawGaussian()
{
  Xpos = randomGaussian() * width;
  Ypos = randomGaussian() * height;
  Csize = randomGaussian() * 100;
  
  circle(Xpos, Ypos, Csize);
  fill(random(0, 255), random(0, 255), random(0, 255));
  stroke(0);
}

//Resets the Sketch after 100 Frames [ 0 - 99 ]
void SketchReset()
{
  if(frameCount >= 100)
  {
    setup();
    frameCount = 0;
  }
}
